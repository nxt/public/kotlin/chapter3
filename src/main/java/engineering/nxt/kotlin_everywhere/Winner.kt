package engineering.nxt.kotlin_everywhere

enum class Winner {
  NoOne, Computer, Player
}
