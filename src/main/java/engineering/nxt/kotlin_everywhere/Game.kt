package engineering.nxt.kotlin_everywhere

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Game(
    @Id
    @GeneratedValue
    val id: Int? = null,
    val userChoice: Choice,
    val computerChoice: Choice,
    val userScore: Long,
    val computerScore: Long)
