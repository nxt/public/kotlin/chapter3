package engineering.nxt.kotlin_everywhere

enum class Choice {
  Rock, Paper, Scissors
}
